- Apache NiFi — http://localhost:8091/nifi/
- Apache NiFi Registry — http://localhost:18080/nifi-registry/
- Apache Airflow — http://localhost:8085/admin/
- pgAdmin — http://localhost:5050/browser/
- minIO — http://localhost:9000/
- superset http://localhost:8088/

Set up Superset:

Run all containers and run

`docker-compose -f ./docker-compose.yml exec superset superset-init`

Enter info to log in to super set
